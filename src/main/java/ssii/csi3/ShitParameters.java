package ssii.csi3;

public class ShitParameters {
    public ShitParameters(String key, String algorithm, String cipherAlgorithm) {
        this.algorithm = algorithm;
        this.cipherAlgorithm = cipherAlgorithm;
        this.key = key;
        this.initVector = "";
    }

    public ShitParameters(String key, String initVector, String algorithm, String cipherAlgorithm) {
        this.algorithm = algorithm;
        this.initVector = initVector;
        this.cipherAlgorithm = cipherAlgorithm;
        this.key = key;
    }

    public String algorithm;
    public String cipherAlgorithm;
    public String key;
    public String initVector;
}
