package ssii.csi3;

import javax.crypto.Cipher;
import java.util.ArrayList;
import java.util.List;

public class Main {
    private static final String ORIGINAL_FILE = "/home/galvesband/sample.avi";
    private static final String CIPHERED_FILE = "/home/galvesband/encripted.thing";
    private static final String DECIPHERED_FILE = "/home/galvesband/decripted.avi";
    private static final String KEY_FILE = "/home/galvesband/secret.key";

    /* Toda implementación de Java tiene que tener estos
        AES/CBC/NoPadding (128)
        AES/CBC/PKCS5Padding (128)
        AES/ECB/NoPadding (128)
        AES/ECB/PKCS5Padding (128)
        DES/CBC/NoPadding (56)
        DES/CBC/PKCS5Padding (56)
        DES/ECB/NoPadding (56)
        DES/ECB/PKCS5Padding (56)
        DESede/CBC/NoPadding (168)
        DESede/CBC/PKCS5Padding (168)
        DESede/ECB/NoPadding (168)
        DESede/ECB/PKCS5Padding (168)
        RSA/ECB/PKCS1Padding (1024, 2048)
        RSA/ECB/OAEPWithSHA-1AndMGF1Padding (1024, 2048)
        RSA/ECB/OAEPWithSHA-256AndMGF1Padding (1024, 2048)
     */
    private static final String CIPHER_MODE = "AES/CBC/PKCS5Padding";
    private static final String ALGORITHM = "AES";
    private static final String INIT_VECTOR = "RandomInitVector"; // 16 bytes para AES
    private static final String KEY =         "OtherRandom16byt";

    public static void main(String[] args) {
        try {
            List<ShitParameters> parameters = new ArrayList<>();
            parameters.add(new ShitParameters("SomeRandomKey16b", "RandomInitVector", "AES", "AES/CBC/PKCS5Padding"));
            parameters.add(new ShitParameters("SecretKey", "DESede", "DESede/ECB/PKCS5Padding"));

            for (ShitParameters params : parameters) {
                Cipher cifrador = CipherUtils.createEncriptingCipher(params);
                CipherUtils.cipherFile(ORIGINAL_FILE, CIPHERED_FILE, cifrador);

                Cipher descifrador = CipherUtils.createDecriptingCipher(params);
                CipherUtils.decipherFile(CIPHERED_FILE, DECIPHERED_FILE, descifrador);

                // Calcular hash del original
                String hash = FileHashingUtils.byteArrayToHexString(FileHashingUtils.getHashOfFile(ORIGINAL_FILE, "MD5"));
                if (FileHashingUtils.compareHashOfFile(DECIPHERED_FILE, hash, "MD5")) {
                    System.out.println("All OK!");
                    System.exit(0);
                }
                else {
                    System.err.println("ERROR: Un-encrypted file is not the same as original file!");
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }
}
