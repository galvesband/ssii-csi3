package ssii.csi3;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;

public class CipherUtils {
    // TODO Es necesario un tamaño de bloque concreto?
    public static final int blockSize = 1024;

    public static Cipher createEncriptingCipher(ShitParameters parameters) throws Exception {
        if (parameters.cipherAlgorithm.contains("/CBC/")) {
            return createEncriptingCBCCipher(parameters.key, parameters.initVector, parameters.algorithm, parameters.cipherAlgorithm);
        }
        else {
            return createEncriptingEBCCipher(parameters.key, parameters.algorithm, parameters.cipherAlgorithm);
        }
    }

    public static Cipher createDecriptingCipher(ShitParameters parameters) throws Exception {
        if (parameters.cipherAlgorithm.contains("/CBC/")) {
            return createDecriptingCBCCipher(parameters.key, parameters.initVector, parameters.algorithm, parameters.cipherAlgorithm);
        }
        else {
            return createDecriptingEBCCipher(parameters.key, parameters.algorithm, parameters.cipherAlgorithm);
        }
    }

    public static Cipher createEncriptingCBCCipher(String key, String initVector, String algorithm, String cipherAlgorithm) throws Exception {
        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);

        Cipher rval = Cipher.getInstance(cipherAlgorithm);
        rval.init(Cipher.ENCRYPT_MODE, keySpec, iv);
        return rval;
    }

    public static Cipher createDecriptingCBCCipher(String key, String initVector, String algorithm, String cipherAlgorithm) throws Exception {
        IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);

        Cipher rval = Cipher.getInstance(cipherAlgorithm);
        rval.init(Cipher.DECRYPT_MODE, keySpec, iv);
        return rval;
    }

    public static Cipher createEncriptingEBCCipher(String key, String algorithm, String cipherAlgorithm) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
        Cipher rval = Cipher.getInstance(cipherAlgorithm);
        rval.init(Cipher.ENCRYPT_MODE, keySpec);
        return rval;
    }

    public static Cipher createDecriptingEBCCipher(String key, String algorithm, String cipherAlgorithm) throws Exception {
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
        Cipher rval = Cipher.getInstance(cipherAlgorithm);
        rval.init(Cipher.DECRYPT_MODE, keySpec);
        return rval;
    }

    // Cifrar un archivo
    public static void cipherFile(String inPath, String outPath, Cipher cipher) throws IOException {
        BufferedInputStream inStream = null;
        CipherOutputStream outStream = null;
        try {
            inStream = new BufferedInputStream(new FileInputStream(inPath));
            outStream = new CipherOutputStream(new BufferedOutputStream(new FileOutputStream(outPath)), cipher);
            processFile(inStream, outStream);
        } finally {
            if (inStream != null)
                inStream.close();
            if (outStream != null)
                outStream.close();
        }
    }

    // Descrifrar archivo
    public static void decipherFile(String inPath, String outPath, Cipher cipher) throws IOException {
        CipherInputStream inStream = null;
        BufferedOutputStream outStream = null;
        try {
            inStream = new CipherInputStream(new BufferedInputStream(new FileInputStream(inPath)), cipher);
            outStream = new BufferedOutputStream(new FileOutputStream(outPath));

            processFile(inStream, outStream);
        } finally {
            if (inStream != null)
                inStream.close();
            if (outStream != null)
                outStream.close();
        }
    }

    private static void processFile(InputStream inStream, OutputStream outStream) throws IOException {
        byte[] buffer = new byte[blockSize];
        int readed = inStream.read(buffer);
        while (readed != -1) {
            outStream.write(buffer, 0, readed);
            readed = inStream.read(buffer);
        }
    }
}
