# Instrucciones básicas #

Por si no os acordais o no habeis visto Gradle antes, así va la cosa. Debería de ser más o menos
igual en Windows...

 - Solo hay que tener instalado el JDK de java (uno no muy antiguo, supongo...).
 
 - Para compilar y tal:
 
   ```bash
   $ ./gradlew build
   ```
   
   La primera vez que lo ejecuteis se descargará Gradle y lo inscrustará en el proyecto.
   
   Despues mirad en la carpeta `build`. Vereis subcarpetas con classes compiladas, archivos para
   distribución, el jar por ahí suelto, etc.
   
 - Para ejecutar:
 
   ```bash
   $ ./gradlew run
   ```
   
 - Recomiendo usar el IntelliJ IDEA (versión gratuita/community es más que suficiente). Soporta
   integración con Gradle del tirón y tiene un montón de analizadores de código estático
   que ayudan.